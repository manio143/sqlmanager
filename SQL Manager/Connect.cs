﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace SQL_Manager
{
    public partial class frmConnect : Form
    {
        public SQLData.DataGetter getter;
        public bool? connected = null;
        string usrDataPath = Environment.GetEnvironmentVariable("appdata") + @"\.md.SqlManager\user_data.dat";
        string usrPassPath = Environment.GetEnvironmentVariable("appdata") + @"\.md.SqlManager\user_pass.dat";

        public frmConnect()
        {
            InitializeComponent();
            if (File.Exists(usrDataPath))
            {
                loadData();
                cbRemember.Checked = true;
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnConnect_Click(object sender, EventArgs e)
        {
            if (cbRemember.Checked) saveData();
            else if (File.Exists(usrDataPath)) { File.Delete(usrDataPath); File.Delete(usrPassPath); }
            btnConnect.Text = "Connecting...";
            btnConnect.Enabled = false;
            getter = new SQLData.DataGetter((SQLData.DBType)getDbType(), txtUser.Text, txtPass.Text, txtServer.Text, txtDbase.Text);
            connected = getter.ConnectionAllowed();
            btnConnect.Enabled = true;
            btnConnect.Text = "Connect";
            this.Close();
        }

        private void saveData()
        {
            char[] c = txtPass.Text.ToCharArray();
            byte[] encss = protectPass(c);
            int dbtype = getDbType();
            string data = String.Format("{0}\n{1}\n{2}\n{3}", txtServer.Text, txtDbase.Text, txtUser.Text, dbtype);
            if (!File.Exists(usrDataPath))
            {
                System.IO.FileInfo file = new System.IO.FileInfo(usrDataPath);
                file.Directory.Create();
            }
            File.WriteAllText(usrDataPath, data);
            File.WriteAllBytes(usrPassPath, encss);
        }
        private void loadData()
        {
            string[] parts = File.ReadAllLines(usrDataPath);
            byte[] data = File.ReadAllBytes(usrPassPath);
            char[] pass = unprotectPass(data);

            txtServer.Text = parts[0];
            txtDbase.Text = parts[1];
            txtUser.Text = parts[2];
            setDbType(Convert.ToInt32(parts[3]));
            txtPass.Text = new string(pass);
        }

        private byte[] protectPass(char[] ss)
        {
            byte[] plaintext = Encoding.UTF8.GetBytes(ss);
            // Generate additional entropy (will be used as the Initialization vector)
            //byte[] entropy = new byte[20];
            //using (RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider())
            //{
            //    rng.GetBytes(entropy);
            //}

            byte[] ciphertext = ProtectedData.Protect(plaintext, null,
                DataProtectionScope.CurrentUser);
            return ciphertext;
        }
        private char[] unprotectPass(byte[] encss)
        {
            byte[] ciphertext = encss;
            // Generate additional entropy (will be used as the Initialization vector)
            //byte[] entropy = new byte[20];
            //using (RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider())
            //{
            //    rng.GetBytes(entropy);
            //}

            byte[] plaintext = ProtectedData.Unprotect(ciphertext, null,
                DataProtectionScope.CurrentUser);
            return Encoding.UTF8.GetString(plaintext).ToCharArray();
        }
        private int getDbType()
        {
            if (rdbMSSql.Checked) return 0;
            if (rdbMySql.Checked) return 1;
            return 0;
        }
        private void setDbType(int i)
        {
            rdbMSSql.Checked = false;
            rdbMySql.Checked = false;
            switch(i)
            {
                case 0: rdbMSSql.Checked = true; break;
                case 1: rdbMySql.Checked = true; break;
            }
        }
    }
}
