﻿namespace SQL_Manager
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lblSQLCommand = new System.Windows.Forms.Label();
            this.statusBar = new System.Windows.Forms.StatusStrip();
            this.statusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.statusProgress = new System.Windows.Forms.ToolStripProgressBar();
            this.menuBar = new System.Windows.Forms.MenuStrip();
            this.menuFile = new System.Windows.Forms.ToolStripMenuItem();
            this.menuConnect = new System.Windows.Forms.ToolStripMenuItem();
            this.menuExit = new System.Windows.Forms.ToolStripMenuItem();
            this.menuHelp = new System.Windows.Forms.ToolStripMenuItem();
            this.menuAbout = new System.Windows.Forms.ToolStripMenuItem();
            this.rtSQLCommand = new System.Windows.Forms.RichTextBox();
            this.btnSQLCommand = new System.Windows.Forms.Button();
            this.controlsContainer = new System.Windows.Forms.Panel();
            this.statusClearer = new System.Windows.Forms.Timer(this.components);
            this.statusBar.SuspendLayout();
            this.menuBar.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblSQLCommand
            // 
            this.lblSQLCommand.AutoSize = true;
            this.lblSQLCommand.Location = new System.Drawing.Point(7, 292);
            this.lblSQLCommand.MaximumSize = new System.Drawing.Size(55, 0);
            this.lblSQLCommand.Name = "lblSQLCommand";
            this.lblSQLCommand.Size = new System.Drawing.Size(54, 26);
            this.lblSQLCommand.TabIndex = 0;
            this.lblSQLCommand.Text = "SQL Command";
            this.lblSQLCommand.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // statusBar
            // 
            this.statusBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusLabel,
            this.statusProgress});
            this.statusBar.Location = new System.Drawing.Point(0, 324);
            this.statusBar.Name = "statusBar";
            this.statusBar.Size = new System.Drawing.Size(479, 24);
            this.statusBar.TabIndex = 3;
            this.statusBar.Text = "statusStrip1";
            // 
            // statusLabel
            // 
            this.statusLabel.AutoSize = false;
            this.statusLabel.Name = "statusLabel";
            this.statusLabel.Size = new System.Drawing.Size(350, 19);
            this.statusLabel.Text = "Ready.";
            this.statusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // statusProgress
            // 
            this.statusProgress.Margin = new System.Windows.Forms.Padding(5, 3, 1, 3);
            this.statusProgress.Name = "statusProgress";
            this.statusProgress.Size = new System.Drawing.Size(100, 18);
            // 
            // menuBar
            // 
            this.menuBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuFile,
            this.menuHelp});
            this.menuBar.Location = new System.Drawing.Point(0, 0);
            this.menuBar.Name = "menuBar";
            this.menuBar.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.menuBar.Size = new System.Drawing.Size(479, 24);
            this.menuBar.TabIndex = 4;
            // 
            // menuFile
            // 
            this.menuFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuConnect,
            this.menuExit});
            this.menuFile.Name = "menuFile";
            this.menuFile.Size = new System.Drawing.Size(37, 20);
            this.menuFile.Text = "&File";
            // 
            // menuConnect
            // 
            this.menuConnect.Name = "menuConnect";
            this.menuConnect.Size = new System.Drawing.Size(119, 22);
            this.menuConnect.Text = "&Connect";
            this.menuConnect.Click += new System.EventHandler(this.menuConnect_Click);
            // 
            // menuExit
            // 
            this.menuExit.Name = "menuExit";
            this.menuExit.Size = new System.Drawing.Size(119, 22);
            this.menuExit.Text = "&Exit";
            this.menuExit.Click += new System.EventHandler(this.menuExit_Click);
            // 
            // menuHelp
            // 
            this.menuHelp.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuAbout});
            this.menuHelp.Name = "menuHelp";
            this.menuHelp.Size = new System.Drawing.Size(44, 20);
            this.menuHelp.Text = "&Help";
            // 
            // menuAbout
            // 
            this.menuAbout.Name = "menuAbout";
            this.menuAbout.Size = new System.Drawing.Size(107, 22);
            this.menuAbout.Text = "&About";
            this.menuAbout.Click += new System.EventHandler(this.menuAbout_Click);
            // 
            // rtSQLCommand
            // 
            this.rtSQLCommand.DetectUrls = false;
            this.rtSQLCommand.Location = new System.Drawing.Point(63, 286);
            this.rtSQLCommand.Name = "rtSQLCommand";
            this.rtSQLCommand.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.rtSQLCommand.Size = new System.Drawing.Size(354, 37);
            this.rtSQLCommand.TabIndex = 5;
            this.rtSQLCommand.Text = "";
            // 
            // btnSQLCommand
            // 
            this.btnSQLCommand.Location = new System.Drawing.Point(419, 286);
            this.btnSQLCommand.Name = "btnSQLCommand";
            this.btnSQLCommand.Size = new System.Drawing.Size(55, 35);
            this.btnSQLCommand.TabIndex = 6;
            this.btnSQLCommand.Text = "Execute";
            this.btnSQLCommand.UseVisualStyleBackColor = true;
            this.btnSQLCommand.Click += new System.EventHandler(this.btnSQLCommand_Click);
            // 
            // controlsContainer
            // 
            this.controlsContainer.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.controlsContainer.Location = new System.Drawing.Point(0, 22);
            this.controlsContainer.Name = "controlsContainer";
            this.controlsContainer.Size = new System.Drawing.Size(479, 260);
            this.controlsContainer.TabIndex = 7;
            // 
            // statusClearer
            // 
            this.statusClearer.Interval = 8000;
            this.statusClearer.Tick += new System.EventHandler(this.statusClearer_Tick);
            // 
            // frmMain
            // 
            this.AcceptButton = this.btnSQLCommand;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(479, 348);
            this.Controls.Add(this.controlsContainer);
            this.Controls.Add(this.btnSQLCommand);
            this.Controls.Add(this.rtSQLCommand);
            this.Controls.Add(this.statusBar);
            this.Controls.Add(this.menuBar);
            this.Controls.Add(this.lblSQLCommand);
            this.MainMenuStrip = this.menuBar;
            this.MinimumSize = new System.Drawing.Size(350, 300);
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SQL Manager";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMain_FormClosing);
            this.Resize += new System.EventHandler(this.frmMain_Resize);
            this.statusBar.ResumeLayout(false);
            this.statusBar.PerformLayout();
            this.menuBar.ResumeLayout(false);
            this.menuBar.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblSQLCommand;
        private System.Windows.Forms.StatusStrip statusBar;
        private System.Windows.Forms.ToolStripStatusLabel statusLabel;
        private System.Windows.Forms.MenuStrip menuBar;
        private System.Windows.Forms.RichTextBox rtSQLCommand;
        private System.Windows.Forms.ToolStripProgressBar statusProgress;
        private System.Windows.Forms.ToolStripMenuItem menuFile;
        private System.Windows.Forms.ToolStripMenuItem menuConnect;
        private System.Windows.Forms.ToolStripMenuItem menuExit;
        private System.Windows.Forms.ToolStripMenuItem menuHelp;
        private System.Windows.Forms.ToolStripMenuItem menuAbout;
        private System.Windows.Forms.Button btnSQLCommand;
        private System.Windows.Forms.Panel controlsContainer;
        private System.Drawing.Size CurrentSize;
        private System.Windows.Forms.DataGridView dataGrid;
        private System.Windows.Forms.Timer statusClearer;
        private System.Windows.Forms.Label lblInnerMsg;
    }
}

