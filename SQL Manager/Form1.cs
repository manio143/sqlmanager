﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SQL_Manager
{
    public partial class frmMain : Form
    {
        Dictionary<String, List<Object>> Data;
        SQLData.DataGetter dataGetter;

        public frmMain()
        {
            InitializeComponent();
            CurrentSize = new Size();
            setCurrentSize(this.Size);

            lblInnerMsg = new Label();
            lblInnerMsg.AutoSize = true;
            lblInnerMsg.Location = new System.Drawing.Point(0, 0);
            lblInnerMsg.Name = "lblInnerMsg";
            lblInnerMsg.Text = "Records affected: ";
            lblInnerMsg.Visible = false;
            this.controlsContainer.Controls.Add(lblInnerMsg);
        }

        private void setCurrentSize(Size s)
        {
            CurrentSize.Height = s.Height;
            CurrentSize.Width = s.Width;
        }

        private void frmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            //disconnect
        }

        private void frmMain_Resize(object sender, EventArgs e)
        {
            Size s = this.Size;
            int h = s.Height - CurrentSize.Height;
            int w = s.Width - CurrentSize.Width;

            controlsContainer.Height += h;
            controlsContainer.Width += w;

            rtSQLCommand.Width += w;
            rtSQLCommand.Location = new Point(rtSQLCommand.Location.X, rtSQLCommand.Location.Y + h);
            btnSQLCommand.Location = new Point(btnSQLCommand.Location.X + w, btnSQLCommand.Location.Y + h);
            lblSQLCommand.Location = new Point(lblSQLCommand.Location.X, lblSQLCommand.Location.Y + h);

            statusLabel.Width += w;

            setCurrentSize(s);
        }

        private void menuConnect_Click(object sender, EventArgs e)
        {
            frmConnect connect = new frmConnect();
        Show:
            connect.connected = null;
            connect.ShowDialog(this);
            if (!connect.connected.HasValue) return;
            if (!connect.connected.Value)
            {
                var result = MessageBox.Show(this, "Unable to connect to the server\n using data provided.\n\n" + connect.getter.Error, "Cannot connect", MessageBoxButtons.RetryCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                if (result == System.Windows.Forms.DialogResult.Retry) goto Show;
                return;
            }
            statusProgress.Value = 50;
            dataGetter = connect.getter;
            statusLabel.Text = "Connected.";
            statusProgress.Value = 0;
        }

        private void menuExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void setGrid()
        {
            if (dataGrid != null) this.dataGrid.Dispose();
            lblInnerMsg.Visible = false;
            var grid = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(grid)).BeginInit();
            grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            grid.Dock = System.Windows.Forms.DockStyle.Fill;
            grid.Location = new System.Drawing.Point(0, 0);
            grid.Name = "dataGrid";
            grid.Size = new System.Drawing.Size(475, 256);
            grid.TabIndex = 0;
            ((System.ComponentModel.ISupportInitialize)(grid)).EndInit();

            List<List<Object>> rows = new List<List<Object>>();
            int rowsCount = getRowsCount();
            for (int i = 0; i < rowsCount; ++i)
                rows.Add(new List<object>());
            foreach (var column in Data)
            {
                grid.Columns.Add(column.Key, column.Key);
                for (int i = 0; i < rowsCount; ++i)
                { rows[i].Add(column.Value[i]); }
            }
            foreach (var row in rows)
            {
                grid.Rows.Add(row.ToArray());
            }
            if (this.controlsContainer.Controls.Contains(dataGrid)) this.controlsContainer.Controls.Remove(dataGrid);
            this.dataGrid = grid;
            this.controlsContainer.Controls.Add(this.dataGrid);
        }
        private void fillData(Dictionary<string, List<Object>> data)
        {
            Data = data;
        }
        private int getRowsCount()
        {
            if (Data != null)
                foreach (var c in Data)
                    return c.Value.ToArray().Length;
            return 0;
        }

        private void btnSQLCommand_Click(object sender, EventArgs e)
        {
            if (dataGetter == null) { MessageBox.Show(this, "No connection has been established.", "No connection"); return; }
            if (dataGetter.ConnectionAllowed())
            {
                try
                {
                    statusProgress.Value = 10;
                    statusLabel.Text = "Getting data...";

                    var data = dataGetter.GetData(rtSQLCommand.Text);

                    statusProgress.Value = 50;
                    fillData(data);
                    statusProgress.Value = 60;
                    setGrid();
                    statusProgress.Value = 100;
                    statusLabel.Text = "Task completed.";
                    clearStatus();
                }
                catch (System.Data.SqlClient.SqlException)
                {
                    MessageBox.Show(this, "An error occured while executing your command.\n\n" + dataGetter.Error, "Connection problem");
                    statusLabel.Text = "Unable to complete the request. Check your SQL command.";
                    clearStatus();
                }
                catch (MySql.Data.MySqlClient.MySqlException)
                {
                    MessageBox.Show(this, "An error occured while executing your command.\n\n" + dataGetter.Error, "Connection problem");
                    statusLabel.Text = "Unable to complete the request. Check your SQL command.";
                    clearStatus();
                }
                catch (Exception ex)
                {
                    if (ex.Data.Count > 0)
                    {
                        displayLabel(Convert.ToInt32(ex.Data["RecordsAffected"]));
                        statusProgress.Value = 100;
                        statusLabel.Text = "Task completed.";
                        clearStatus();
                    }
                    else
                    {
                        statusLabel.Text = "An unknown error occured.";
                        statusProgress.Value = 0;
                    }
                }
            }
            else
            {
                MessageBox.Show(this, "Unable to connect to the database.", "Connection problem");
                diconnect();
            }
        }

        private void displayLabel(int count)
        {
            if (dataGrid != null) this.dataGrid.Dispose();
            lblInnerMsg.Text = "Records affected: " + count.ToString();
            lblInnerMsg.Visible = true;
        }

        private void clearStatus()
        {
            statusClearer.Enabled = true;
        }
        void diconnect()
        {
            if (dataGetter != null)
                dataGetter = null;
        }

        private void statusClearer_Tick(object sender, EventArgs e)
        {
            statusClearer.Enabled = false;
            statusLabel.Text = "Ready.";
            statusProgress.Value = 0;
        }

        private void menuAbout_Click(object sender, EventArgs e)
        {
            using (Form about = new frmAbout())
            {
                about.ShowDialog(this);
            }
        }
    }
}
namespace SQLData
{
    using SQLControl;
    using MySql.Data.MySqlClient;
    public class DataGetter
    {
        SQLController control;
        MySqlConnection mysqlcnt;
        DBType dbtype;

        public string Error;
        public DataGetter(DBType type, string userId, string password, string server, string dbase)
        {
            if (type == DBType.MSSQL)
                control = new SQLController(userId, password, server, dbase, 6);
            else if (type == DBType.MySQL)
                mysqlcnt = new MySqlConnection("Server=" + server + ";Database=" + dbase + ";Uid=" + userId + ";Pwd=" + password + ";");
            this.dbtype = type;
        }
        public bool ConnectionAllowed()
        {
            Error = "";
            try
            {
                if (dbtype == DBType.MSSQL)
                    control.ConnectionOpen();
                else if (dbtype == DBType.MySQL)
                    mysqlcnt.Open();
                return true;
            }
            catch (Exception e) { Error = e.Message; return false; }
            finally
            {
                if (dbtype == DBType.MSSQL)
                    control.ConnectionClose();
                else if (dbtype == DBType.MySQL)
                    mysqlcnt.Close();
            }
        }
        public Dictionary<String, List<Object>> GetData(string sqlCommand)
        {
            Error = "";
            Dictionary<String, List<Object>> data = new Dictionary<String, List<Object>>();
            List<string> colTitle = null;
            List<List<Object>> fields = null;
            try
            {
                if (dbtype == DBType.MSSQL)
                {
                    control.ConnectionOpen();
                    control.SetCommand(sqlCommand);
                    System.Data.SqlClient.SqlDataReader reader = control.ExecuteCommandReader();
                    if (!reader.HasRows)
                    {
                        Exception ex = new Exception("There's nothing to show.");
                        ex.Data.Add("RecordsAffected", reader.RecordsAffected);
                        throw ex;
                    }

                    colTitle = new List<string>();
                    for (int i = 0; i < reader.FieldCount; ++i)
                        if (!colTitle.Contains(reader.GetName(i)))
                            colTitle.Add(reader.GetName(i));

                    fields = new List<List<Object>>();
                    for (int i = 0; i < colTitle.Count; ++i)
                        fields.Add(new List<Object>());

                    while (reader.Read())
                    {
                        int i = 0;
                        foreach (var field in fields)
                        {
                            field.Add(reader[colTitle[i]]);
                            i++;
                        }
                    }
                    reader.Close();
                }
                else if (dbtype == DBType.MySQL)
                {
                    mysqlcnt.Open();
                    MySqlCommand cmd = new MySqlCommand(sqlCommand, mysqlcnt);
                    var reader = cmd.ExecuteReader();

                    if (!reader.HasRows)
                    {
                        Exception ex = new Exception("There's nothing to show.");
                        ex.Data.Add("RecordsAffected", reader.RecordsAffected);
                        throw ex;
                    }

                    colTitle = new List<string>();
                    for (int i = 0; i < reader.FieldCount; ++i)
                        if (!colTitle.Contains(reader.GetName(i)))
                            colTitle.Add(reader.GetName(i));

                    fields = new List<List<Object>>();
                    for (int i = 0; i < colTitle.Count; ++i)
                        fields.Add(new List<Object>());

                    while (reader.Read())
                    {
                        int i = 0;
                        foreach (var field in fields)
                        {
                            field.Add(reader[colTitle[i]]);
                            i++;
                        }
                    }

                    reader.Close();
                }
            }
            catch (Exception e) { Error = e.Message; throw e; }
            finally
            {
                if (dbtype == DBType.MSSQL)
                    control.ConnectionClose();
                else if (dbtype == DBType.MySQL)
                    mysqlcnt.Close();
            }
            int header = 0;
            foreach (var col in fields)
            {
                data.Add(colTitle[header], fields[header]);
                header++;
            }

            return data;
        }
        public Task<Dictionary<String, List<Object>>> GetDataAsync(string sqlCommand)
        {
            Func<Dictionary<String, List<Object>>> f = () => GetData(sqlCommand);
            return new Task<Dictionary<String, List<Object>>>(f);
        }
    }
    public enum DBType
    {
        MSSQL,
        MySQL
    }
}
