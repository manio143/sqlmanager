#SQL Manager#

A simple, easy-to-use program written in C# that allows you to connect to a database with user-password authentication.
Any results from a SELECT statement are shown in a data table.

###Compatibility###
* .NET Framework 4.5
* Visual Studio 2013

###Known Issues###
Sometimes the input panel disappears, don't know why or how to bring it back.

###Compilation###
You can compile it either inside of Visual Studio or by running `msbuild`. You can find it in `C:\Windows\Microsoft.NET\Framework\v4.**`
(I recommend adding it to `$PATH`)

Now simply run `msbuild "SQL Manager.csproj"` in the `SQL Manager` directory.

